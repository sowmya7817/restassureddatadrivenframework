package testDatabase;

import static io.restassured.RestAssured.given;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TC002_insertStudent {
	@BeforeTest
	public void init(){
		RestAssured.baseURI="http://localhost";
		RestAssured.port=8080;
		RestAssured.basePath="/student";
			}
	
	@SuppressWarnings("deprecation")
	@Test
	public void getZipcodes(){
		try{
			Class.forName("com.mysql.jdbc.Driver"); // loads the driver class
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc", "root", "root"); //creates connection with the database
			Statement st = con.createStatement();// create statement
			ResultSet result = st.executeQuery("select * from students where id=110");//execute the statement and return the result into the ResultSet
			String query = "insert into students (firstName,lastName,email,programme,courses) values(?,?,?,?,?)";
			
			while(result.next())//processing the ResultSet
			{
				
			String id = result.getString("id");
			String firstName= result.getString("firstName");
			String lastName = result.getString("lastName");
			String email = result.getString("email");
			String programme = result.getString("programme");
			String courses = result.getString("courses");
			
			System.out.println(id);
			System.out.println(lastName);
				
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, id);
			ps.setString(2, firstName);
			ps.setString(3, lastName);
			ps.setString(4, email);
			ps.setString(5, programme);
			ps.setString(6, courses);
			ps.execute();
	
	     given().contentType(ContentType.JSON)
		.when().body(ps.execute())
		.post().then().statusCode(201);
	
	     result.close();
			}
	st.close();
	con.close();
		}catch(Exception ex){
			System.out.println("Message " + ex);
		}
		
		}
}
