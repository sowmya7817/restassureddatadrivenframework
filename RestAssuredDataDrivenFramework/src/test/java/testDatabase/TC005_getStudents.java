package testDatabase;

import static io.restassured.RestAssured.given;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import dataBaseClass.DataBaseConnection;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import testBase.BaseClass;

public class TC005_getStudents extends BaseClass {
	//@BeforeTest
	public void init(){
		BaseClass bc = new BaseClass();
		RestAssured.baseURI="http://localhost";
		RestAssured.port=8080;
		RestAssured.basePath="/student";
		 			}
	
	@SuppressWarnings("deprecation")
	@Test
	public void getZipcodes(){
		try{
			Class.forName("com.mysql.jdbc.Driver"); // loads the driver class
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc", "root", "root"); //creates connection with the database
			Statement st = con.createStatement();// create statement
			ResultSet result = st.executeQuery("select * from students ");//execute the statement and return the result into the ResultSet
			
	while(result.next())//processing the ResultSet
	{
		String id = result.getString("id");
		String firstName= result.getString("firstName");
		String lastName = result.getString("lastName");
		String email = result.getString("email");
		String programme = result.getString("programme");
		String courses = result.getString("courses");
		
		System.out.println(id);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(email);
		System.out.println(programme);
		System.out.println(courses);
				
	    Response response = given().pathParameters("id",id).get("/{id}");
	    System.out.println(response.body().prettyPrint());
	}
	result.close();
	st.close();
	con.close();
		}catch(Exception ex){
			System.out.println("Message " + ex);
		}
		
		}
	

}
