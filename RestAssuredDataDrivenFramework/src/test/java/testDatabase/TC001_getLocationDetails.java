package testDatabase;

import java.io.IOException;
import static org.hamcrest.Matchers.equalTo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;

import static io.restassured.RestAssured.given;

import dataBaseClass.DataBaseConnection;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TC001_getLocationDetails extends DataBaseConnection {
	
		@BeforeTest
	public void init(){
		RestAssured.baseURI="http://api.zippopotam.us";
		RestAssured.basePath="/us/";
			}
	
	@SuppressWarnings("deprecation")
	@Test
	public void getZipcodes(){
		try{
			Class.forName("com.mysql.jdbc.Driver"); // loads the driver class
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc", "root", "root"); //creates connection with the database
			Statement st = con.createStatement();// create statement
			ResultSet result = st.executeQuery("select * from location ");//execute the statement and return the result into the ResultSet
			
	while(result.next())//processing the ResultSet
	{
		String zipcode = result.getString("zipcode");
		String city= result.getString("city");
		String state = result.getString("state");
		
		System.out.println(zipcode);
		System.out.println(city);
		System.out.println(state);
	    Response response = given().pathParameters("zipcode",zipcode).get("/{zipcode}");
		    
	}
	result.close();
	st.close();
	con.close();
		}catch(Exception ex){
			System.out.println("Message " + ex);
		}
		
		}
	
}

